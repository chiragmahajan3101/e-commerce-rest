<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\ApiController;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();
        return $this->showAll($transactions);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return $this->showOne($transaction);
    }
}
